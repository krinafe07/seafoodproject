
// Components
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
// Admin
import { HomeComponent } from './Components/Admin/home/home.component';
import { UpdateDishComponent } from './Components/Admin/Dishes/add-dish/update-dish/update-dish.component';
import { DeleteDishComponent } from './Components/Admin/Dishes/delete-dish/delete-dish.component';
import { AddDishComponent } from './Components/Admin/Dishes/add-dish/add-dish.component';
import { ReportComponent } from './Components/Admin/Report/report/report.component';
import { AddUserComponent } from './Components/Admin/User/add-user/add-user.component';
import { DeleteUserComponent } from './Components/Admin/User/delete-user/delete-user.component';
import { UpdateUserComponent } from './Components/Admin/User/update-user/update-user.component';
//Client
import { CarShopComponent } from './Components/Client/car-shop/car-shop.component';
import { CatalogComponent } from './Components/Client/catalog/catalog.component';
import { EditPerfilComponent } from './Components/Client/edit-perfil/edit-perfil.component';
import { FoodOrderComponent } from './Components/Client/food-order/food-order.component';
import { PayOrderComponent } from './Components/Client/pay-order/pay-order.component';
// Modules
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';




@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ReportComponent,
    CarShopComponent,
    CatalogComponent,
    EditPerfilComponent,
    FoodOrderComponent,
    PayOrderComponent,
    AddUserComponent,
    DeleteUserComponent,
    UpdateUserComponent,
    UpdateDishComponent,
    DeleteDishComponent,
    AddDishComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
